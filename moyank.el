;;; moyank.el --- Yank utilities -*- lexical-binding: t -*-

;; Author: moritz
;; Maintainer: moritz
;; Version: 0.0.0
;; Package-Requires: ((cl-seq) (subr-x))
;; Homepage: https://mo-profile.com
;; Keywords: yank


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Yank utilities for emacs

;;; Code:

;;; moyank.el ends here

(require 'cl-seq)
(require 'subr-x)

(defcustom moyank--beginning-string
  ""
  "String to add in beginning of the yank list"
  :type 'string
  :group 'moyank)

(defcustom moyank--end-string
  ""
  "String to add in the end of the yank list"
  :type 'string
  :group 'moyank)

(defcustom moyank--default-separator
  ", "
  "Define the separator to be usend when `moyank--prompt-separator' is nil and
as default separator"
  :type 'string
  :group 'moyank)

(defvar moyank--prompt-separator
  nil
  "Define wheather the separator will be asked or the default will be used")

(defvar moyank--kill-counter
  0
  "The number of recent kills")

(defvar moyank--last-kill-counter
  0
  "The number of recent kills")

(defvar moyank--timer
  nil
  "Hold the timer to cancel if needed")

(defcustom moyank--yank-counter-timeout-in-seconds 10
  "Time to hold the counter until next kill"
  :group 'moyank
  :type 'integer)

(defun moyank--kill-counter-advice (orig-fun &rest args)
  (setq moyank--kill-counter (1+ moyank--kill-counter))
  (setq moyank--last-kill-counter moyank--kill-counter)
  (if moyank--timer
      (cancel-timer moyank--timer))
  (setq moyank--timer
        (run-at-time moyank--yank-counter-timeout-in-seconds
                     nil
                     'moyank--reset-kill-counter))
  (moyank--message-kill-counter)
  (apply orig-fun args))

(defun moyank--message-kill-counter ()
  (message "kills: %d" moyank--kill-counter))

(defun moyank-reset-kill-counter ()
  (interactive)
  (moyank--reset-kill-counter))

(defun moyank--reset-kill-counter ()
  (cancel-timer moyank--timer)
  (setq moyank--kill-counter 0)
  (moyank--message-kill-counter))

(advice-add 'moritz/kill-ring-save :around #'moyank--kill-counter-advice)

(defun moyank-toggle-prompt-separator ()
  (interactive)
  (if moyank--prompt-separator
      (setq moyank--prompt-separator nil)
    (setq moyank--prompt-separator t)))

(defun moyank-toggle-add-separator-beginning ()
  (interactive)
  (if moyank--beginning-string
      (setq moyank--beginning-string nil)
    (setq moyank--beginning-string t)))

(defun moyank-set-default-separator (separator)
  (interactive "s")
  (setq moyank--default-separator separator))

(defun moyank(n)
  (interactive "p")
  (if (not (= n 1))
      (let ((sub-kill-ring (reverse (cl-subseq kill-ring 0 n)))
            (separator moyank--default-separator))
        (if moyank--prompt-separator
            (setq separator (read-string (format "Separator (%s): " separator) nil t separator)))
        (insert (concat
                 moyank--beginning-string
                 (string-trim (mapconcat 'identity sub-kill-ring separator))
                 moyank--end-string)))
    (yank)))

(defun moyank-complex ()
  (interactive)
  (let* ((n (read-number "How many yanks: " moyank--last-kill-counter))
         (sub-kill-ring (reverse (cl-subseq kill-ring 0 n)))
         (separator (read-string (format "Separator (%s): " moyank--default-separator) nil t moyank--default-separator))
         (moyank--beginning-string
          (read-string "String to add in the beginning: " nil t moyank--beginning-string))
         (moyank--end-string
          (read-string "String to add in the end: " nil t moyank--end-string)))
    (insert
     (concat
      moyank--beginning-string
      (string-trim (mapconcat 'identity sub-kill-ring separator))
      moyank--end-string))))

(provide 'moyank)
